package smoslt.util;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.btrg.uti.LoadProperties_;
import org.btrg.uti.WriteProperties_;

public class ConfigProperties {
	public final static String CONFIG_FILE_PATH = "./META-INF/config.properties";
	public static final String HEADER = "CONFIGURATION FILE FOR MAIN LAUNCH CLI - SMOSLT";
	public static final String PROJECT_NAME = "PROJECT_NAME";
	public static final String OUTPUT_TIMESTAMP = "OUTPUT_TIMESTAMP";
	public static final String DOWNLOAD_FOLDER = "DOWNLOAD_FOLDER";
	public static final String MUNGE_FILE_PATH = "MUNGE_FILE_PATH";
	public static final String GENERATE_FOLDER = "GENERATE_FOLDER";
	public static final String TEST_HARNESS_SOURCE_DIR = "TEST_HARNESS_SOURCE_DIR";
	public static final String SIDE_EFFECT_IDE_COMPILE_DIR_LOCAL = "SIDE_EFFECT_IDE_COMPILE_DIR_LOCAL";
	public static final String SIDE_EFFECT_IDE_COMPILE_DIR_CD = "SIDE_EFFECT_IDE_COMPILE_DIR_CD";
	public static final String CSV_OUTPUT_DATA_FILE_NAME = "CSV_OUTPUT_DATA_FILE_NAME";
	public static final String EXCEL_OUTPUT_DATA_FILE_NAME = "EXCEL_OUTPUT_DATA_FILE_NAME";
	static Properties properties;

	public static Properties get() {
		if (null == properties) {
			initializeProperties();
		}
		return properties;
	}

	static Properties defaultProperties() {
		properties = new Properties();
		properties.setProperty(PROJECT_NAME, "myProject");
		properties.setProperty(OUTPUT_TIMESTAMP, "true");
		properties.setProperty(DOWNLOAD_FOLDER, "~/Downloads");
		properties.setProperty(MUNGE_FILE_PATH, "./META-INF/munge.csv");
		properties.setProperty(GENERATE_FOLDER, "./generated");
		properties.setProperty(TEST_HARNESS_SOURCE_DIR, "src/test/resources");
		properties.setProperty(SIDE_EFFECT_IDE_COMPILE_DIR_LOCAL,
				"../smoslt.given/target/classes/smoslt/given/sideeffect");
		properties.setProperty(SIDE_EFFECT_IDE_COMPILE_DIR_CD,
				"target/classes/smoslt/given/sideeffect");
		properties.setProperty(CSV_OUTPUT_DATA_FILE_NAME, "output.csv");
		properties.setProperty(EXCEL_OUTPUT_DATA_FILE_NAME, "output.xls");
		// properties.setProperty("", "");
		// properties.setProperty("", "");
		// properties.setProperty("", "");
		// properties.setProperty("", "");
		return properties;
	}

	static void initializeProperties() {
		File configFileLocation = new File(CONFIG_FILE_PATH);
		if (configFileLocation.exists()) {
			properties = LoadProperties_.instance().getProperties(
					CONFIG_FILE_PATH);
		} else {
			defaultProperties();
			WriteProperties_.instance().write(CONFIG_FILE_PATH, properties,
					ConfigProperties.HEADER);
		}
		System.out.println("CONSUMING CONFIG FILE AT "+ configFileLocation.getAbsolutePath());
	}

}
