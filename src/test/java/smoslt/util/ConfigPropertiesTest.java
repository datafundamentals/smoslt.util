package smoslt.util;

import static org.junit.Assert.*;

import org.junit.Test;

import smoslt.util.ConfigProperties;

public class ConfigPropertiesTest {

	@Test
	public void testInitializeProperties() {
		ConfigProperties.initializeProperties();
		assertNotNull(ConfigProperties.properties);
		assertEquals(10,ConfigProperties.get().size());
	}

}
